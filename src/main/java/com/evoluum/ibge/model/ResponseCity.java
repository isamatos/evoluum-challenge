package com.evoluum.ibge.model;

public class ResponseCity {
    private int id;

    public ResponseCity(final int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
