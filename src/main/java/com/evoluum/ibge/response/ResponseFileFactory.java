package com.evoluum.ibge.response;

public class ResponseFileFactory {
    public static ResponseFile getResponseFile(final String fileType) {
        switch (fileType) {
            case "csv":
                return new ResponseFileCsv();

            case "json":
                return new ResponseFileJson();
        }
        return null;
    }
}
