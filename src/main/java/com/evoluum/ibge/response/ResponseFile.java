package com.evoluum.ibge.response;

import com.evoluum.ibge.model.ResponseLocality;

import javax.servlet.http.HttpServletResponse;
import java.util.Set;

public interface ResponseFile {
    void buildResponse(Set<ResponseLocality> responseLocalities, HttpServletResponse httpResponse);
}
